{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c0194b10",
   "metadata": {},
   "source": [
    "# Automating DFT calculations\n",
    "\n",
    "In this notebook, we will illustrate the use of Python to run a bactch of electronic-structure calculations using some program. For simplicity, we will use `PySCF` as a command-line tool. The process would be similar, really, with any simulation software with a CLI (command-line interface) that reads input files and saves its data as text files. Better yet, the Hylleraas Software platform's purpose is precisely to help integrating simulations and analysis, but this course is about learning Python in general, and seeing how projects like HSP may work is very useful. \n",
    "\n",
    "Therefore, the following notebook example is *simple*, and probably not *optimal*, but you will learn a few Python things in the process. Indeed, PySCF is a Python package, and it is really better to run the calculations directly in this notebook. However, what you learn here is the general process of reading and writing text files and running external programs.\n",
    "\n",
    "In our example we will read a small dataset of XYZ geometries of molecules stored in a YAML file, [`xyz_dataset.yml`](xyz_dataset.yml) (click to download if you are viewing this notebook from gitlab.io, otherwise to to https://hylleraas-python-school.gitlab.io/chapters/lecture3/intro.html). YAML is a language for data, fairly common, and easy to work with in Python. PySCF also reads and writes data in this format. The file format is indentation aware, just like Python. The molecule geometries are experimental ones, and you will compute B3LYP ground state energies for each molecule.\n",
    "\n",
    "A nice introduction to YAML files can be found [here](https://www.pythonforbeginners.com/basics/convert-python-dictionary-to-yaml).\n",
    "\n",
    "\n",
    "\n",
    "## Installing PySCF\n",
    "\n",
    "We need to install PySCF and its CLI, which is really as easy as doing the following in the shell:\n",
    "```bash\n",
    "conda install pip\n",
    "pip install --prefer-binary pyscf\n",
    "pip install git+https://github.com/pyscf/pyscfcli\n",
    "```\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9bae6460",
   "metadata": {},
   "source": [
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9e2a3dd",
   "metadata": {},
   "source": [
    "### Python dictionaries\n",
    "\n",
    "In this notebook, the dataset, as well as input and output to PySCF is handled using YAML files. YAML files are read into `dict` objects -- dictionaries. A dictionary is a data type that is a collection of KEY (a string) and VALUE (any Python object) pairs. In the next few cells, we get acquainted with dictionaries.\n",
    "\n",
    "**Task:** Look closely at the following code. It describes a simple dictionary object. Notice how the KEYs are strings, while the VALUEs are various things. Add a key/value pair to `peppa`, and modify the code to print your added data as well.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2556f752",
   "metadata": {},
   "outputs": [],
   "source": [
    "# example dictionary. \n",
    "# [Modify this line to add a key/value pair:]\n",
    "peppa = {'name': 'Peppa Pig', 'age': 4}\n",
    "\n",
    "# show the dictionary\n",
    "print(peppa)\n",
    "\n",
    "# get something from the dictionary\n",
    "the_name = peppa['name']\n",
    "the_age = peppa['age']\n",
    "\n",
    "print(f'{the_name} is {the_age} years old.')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25e48d42",
   "metadata": {},
   "source": [
    "### Nesting Python dictionaries\n",
    "\n",
    "Since the VALUEs of dictionaries can be any object, they can be dictionaries as well! Thus, a nested dictionary is a *type of tree structure*. \n",
    "\n",
    "**Task:** Look carefuly at the following code, and run it. No other tasks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5502912e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a nested dictionary.\n",
    "george = {'name': 'George Pig', 'age': 2, 'sister': peppa}\n",
    "\n",
    "print(george)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7ab0099",
   "metadata": {},
   "source": [
    "### Creating a YAML file from a dictionary\n",
    "\n",
    "A YAML file is basically an indented text representation of a dictionary. Thus, a dictionary can be converted to a YAML file and vice versa. In the next cell, we convert george to a YAML file (or the contents thereof).\n",
    "\n",
    "**Task:** Look carefully at the code. Run it, and inspect the resulting file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a4f0b135",
   "metadata": {},
   "outputs": [],
   "source": [
    "import yaml\n",
    "\n",
    "yaml_content = yaml.safe_dump(george, sort_keys=False)\n",
    "\n",
    "print(yaml_content)\n",
    "\n",
    "with open('george.yml', 'w') as file:\n",
    "    file.write(yaml_content)\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13e09902",
   "metadata": {},
   "source": [
    "### Reading a YAML file into a dictionary\n",
    "\n",
    "In the previous cell, you wrote a YAML file called `george.yml`. We will now read it back into memory, but first you will edit the file.\n",
    "\n",
    "**Task:** Open `george.yml` in your favorite text editor. Add lines that defines the key 'friend', and define the friend to be the YAML equivalent of the dict `{'name': 'Mr. Dinosaur'}`. Then, run the following code cell to see the results. Remember that indentation matters in YAML files.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eab0b9eb",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('george.yml', 'r') as file:\n",
    "    george_new = yaml.safe_load(file)\n",
    "    \n",
    "print(george_new)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "378c2e7d",
   "metadata": {},
   "source": [
    "### Getting acquainted with the dataset\n",
    "\n",
    "Have a look at the dataset file in your favorite text editor. What does it contain? There is one special character following the key, and that is the pipe `|`, which tells the parser to combine the lines below into a single string.\n",
    "\n",
    "**Task:** Read the file `xyz_dataset.yml` into the variable `xyz_dataset`. Do this by copying the previous cell and modifying it accordingly. Run the cell, and check that the printed output makes sense.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cfb90db7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# open the dataset file, and read its contents into a dict object.\n",
    "# [your code here]\n",
    "\n",
    "\n",
    "# (xyz_data should now contain the output of yaml.safe_load.)\n",
    "\n",
    "\n",
    "# Show the dataset by iterating over each data point\n",
    "for key in xyz_data:\n",
    "    print('Molecule name:', key)\n",
    "    print('Atom positions:')\n",
    "    print(xyz_data[key])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a68df3d3",
   "metadata": {},
   "source": [
    "### Generating a PySCF inout file\n",
    "\n",
    "We will be calling PySCF repeatedly, once for each data point. Thus, we must write an input file for a given data point. The input file is a YAML file, so we here define a function that takes an xyz string as input and writes the corresponding input file.\n",
    "\n",
    "**Task:** Consider the following code carefully. Your task is to complete the function by (a) opening the file `pyscf_input.yml` and (b) saving `inpit_dict` usint `pyaml.safe_dump()`. Look at the cell where we saved George Pig to a YAML file for directions. \n",
    "\n",
    "The last command of the cell applies the function to the `ethane` molecule data point for the sake of testing. Inspect your working directory and look at `pyscf_input.yml` to make sure it look all right. The xyz string may look strange, but don't worry - PyYAML does it correctly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "516d388e",
   "metadata": {},
   "outputs": [],
   "source": [
    "def generate_input(xyz):\n",
    " \n",
    "    input_dict = {\n",
    "        'Mole': {\n",
    "            'atom': xyz,\n",
    "            'basis': 'sto-6g',\n",
    "            'symmetry': False,\n",
    "            'unit': 'a'\n",
    "        },\n",
    "        'KS': {\n",
    "            'init_guess': 'minao',\n",
    "            'conv_tol': 1e-7,\n",
    "            'xc': 'b3lyp',\n",
    "            'results': ['e_tot', 'converged']\n",
    "        }\n",
    "    }\n",
    "    \n",
    "    # [add your code here!]\n",
    "    # ...\n",
    "    # ...\n",
    "    \n",
    "    # end of function\n",
    "\n",
    "        \n",
    "# Test the function on \n",
    "generate_input(xyz_data['ethane'])\n",
    "\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "98c92e61",
   "metadata": {},
   "source": [
    "### Running a calculation\n",
    "\n",
    "So, now we have `pyscf_input.yml` on saved, corresponing to a datapoint. In the following cell, we run the PySCF calculation, and read back the results -- which are saved as yet another YAML file!\n",
    "\n",
    "\n",
    "**Task:** Look carefully at the following code cell, and read the comments. Run the cell. It should complete successfully, perhaps with irrelevant warnings from PySCF. Inspect the output file `pyscf_output.yml`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5572e331",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "# Define the command\n",
    "call = 'pyscf pyscf_input.yml >pyscf_output.yml'\n",
    "\n",
    "# Run the command, and store any exit flag\n",
    "info = os.system(call)\n",
    "\n",
    "# If the exit flag is different from zero, there was an error\n",
    "if info != 0:\n",
    "    # Print message\n",
    "    print('There was an error!')\n",
    "else:\n",
    "    # Read the results section of the output file\n",
    "    print('PySCF did not return an error!')\n",
    "    with open('pyscf_output.yml', 'r') as f:\n",
    "        data = yaml.safe_load(f.read())\n",
    "        results = data['KS']['results']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eea4270f",
   "metadata": {},
   "source": [
    "### Writing a helper function\n",
    "\n",
    "In the next cell, we will write a function that takes an xyz string as input, generates the corresponding input file, runs the PySCF calculation, and reads back the result, as we did in the previous cell.\n",
    "\n",
    "**Task:** Complete the function definition! Look at the previous cell for guidance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c0399b23",
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_single(xyz):\n",
    "    \"\"\" Perform DFT calculations on a single molecule with atom positions\n",
    "    defined by the string xyz. Returns a dictionary of results. \"\"\"\n",
    "    \n",
    "    # Generate input file\n",
    "    generate_input(xyz)\n",
    "    \n",
    "    # [your code here]\n",
    "    # ...\n",
    "    # ...\n",
    "    # ...\n",
    "    \n",
    "    \n",
    "    return results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68dda802",
   "metadata": {},
   "source": [
    "### Running the simulations\n",
    "\n",
    "We are now all set for running our simulations! We will loop over our data set and use the previously defined helper function. The results are stored in memory as a dictionary.\n",
    "\n",
    "**Task:** Look carefully at the following code cell, and run it. The calculations should complete without errors.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b0f5cbc",
   "metadata": {},
   "outputs": [],
   "source": [
    "results = {}\n",
    "for key in xyz_data:\n",
    "    results[key] = compute_single(xyz_data[key])\n",
    "\n",
    "print(results)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7370cd2c",
   "metadata": {},
   "source": [
    "### Saving the results to file\n",
    "\n",
    "Let us save the data! For simplicity we use -- you guessed it -- a YAML file.\n",
    "\n",
    "**Task:** Save the results to the file `results.yml`. There are previous tasks that should guide you here.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3a3ad75e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# [your code here]\n",
    "# ...\n",
    "# ..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84e99ffd",
   "metadata": {},
   "source": [
    "### Saving the results II: An Excel file\n",
    "\n",
    "YMAL files are fine, but what if you want your results in Excel? Will you need to manually copy-paste the results? Of course not! In a previous lession, we saw how Pandas can be used to handle Excel files.\n",
    "\n",
    "For writing to excel to work, I had to install an additional Python package:\n",
    "\n",
    "```\n",
    "pip install openpyxl\n",
    "```\n",
    "\n",
    "**Task:** Complete the following code to save the data to an excel file. Read the comments carefully."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "edc94549",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import Pandas\n",
    "import pandas as pd\n",
    "\n",
    "# Create a DataFrame from the results. You need to modify this line!\n",
    "df = pd.DataFrame.from_dict( .....  ) # [edit this line!]\n",
    "\n",
    "# Display the dataframe\n",
    "display(df)\n",
    "\n",
    "# Save to excel. You need to modify this line so it saves to `results.xlsx`.\n",
    "# You can use help(df.to_excel) in a separate cell and look at the examples!\n",
    "df.to_excel( ...... ) # [edit this line!]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "069ffa94",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

# Hylleraas Python Introduction course

> **Disclaimer:** These pages are currently under development. They are made public prematurely to allow testing of the teaching material.

This repository contains teaching material for the Python introduction seminar series in the fall of 2023. The seminar series is set up with the intention of improving the Python skills to prepare participants for the upcoming [Hylleraas School 2024](https://www.mn.uio.no/hylleraas/english/news-and-events/events/events-in-oslo/2024/hylleraas-school-2024.html).

In this repositoty teaching material is organised using Jupyter Book. With Jupyter book it is easy to combine Markdown (`.md`), reStructuredText (`.rst`), HTML (`.html`), executable scripts (e.g. Python (`.py`)), and Jupyter Notebooks (`.ipynb`).

## How to build

The deployed web-pages are automatically updated when code changes are pushed to the repository. Until the pages have been fully deployed you can go to [this page](https://gitlab.com/hylleraas-python-school/hylleraas-python-course/pages) and click the link direclty under `Access pages` to see the generated pages.

To build and inspect the generated html pages locally, run
```
pip install -r requirements.txt
jupyter-book build .
open _build/html/index.html
```

## How to contribute

The contents of the Jupyter book are easily modified by augmenting the table-of-content file `_toc.ylm`, and the layout of the pages is steered by `_config.ylm`. GitLab specific stuff is in `.gitlab-ci.yml`.

Jupyter Book uses [Sphinx](https://jupyterbook.org/en/stable/explain/sphinx.html), or is a distro of Sphinx. Documentation: [Sphinx book theme](https://sphinx-book-theme.readthedocs.io/en/stable/) and
[the Jupyter Book documentation](https://jupyterbook.org).

# Session 2: Data handling

This section is designed as a quick introduction to data handling and visualisation using the Python module pandas. For both the Beginner and Intermediate paths below, you will need to download the two data files:

{download}`Download the quantum_properties csv file <./tmQM_quantum_properties.csv>`

{download}`Download the additional_data csv file <./tmQM_additional_data.csv>`

and the slides

{download}`Download the lecture slides <./Python_Preschool_Data_Analysis.pdf>`

Path: Beginner

The beginner path is designed for anyone with little or absolutely no coding experience (in Python or any other language).

Path: Intermediate

The intermediate path is designed for anyone with some experience in programming using another language.


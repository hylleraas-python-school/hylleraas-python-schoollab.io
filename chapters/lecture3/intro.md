# Session 3: Automating tasks

In this session, we will go through some examples where Python is used in combination with other software to automate various tasks. 

Notebooks for exercises:
- Automate running quantum chemistry software
    - Running many numerical calculations in a batch and
      processing their results, putting numbers in an Excel sheet,
      can be a big and labor-intensive task.
    - Hylleraas Platform is the perfect tool for this!
    - Dataset: [xyz_dataset.yml](./xyz_dataset.yml)
- Extracting images from PDF file
    - PDF files are an important part of the research landscape!
    - Take a journal article, extract figures etc. automatically
      and save to files. 
    - PDF file: [balcells_skjelstad_tmQM.pdf](./balcells_skjelstad_tmQM.pdf)
- Automation of email sending
    - As an academic, there are instances of repeating 
      boring emails, for example when inviting to workshops.
    - This can be automated! (One should be careful, of course ... )
    - Excel, Outlook (UiO mail), SMTP (e.g., google mail)
    - CSV file: [halloween-party.csv](halloween-party.csv)

Slides for the short lecture can be found [here](lecture3.pptx).







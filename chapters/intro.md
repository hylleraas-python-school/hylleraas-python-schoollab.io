# Welcome to the Hylleraas Python Introduction Seminars

As part of the preparations for the [Hylleraas School 2024](https://www.mn.uio.no/hylleraas/english/news-and-events/events/events-in-oslo/2024/hylleraas-school-2024.html), we are excited to announce a Python introduction seminar series. These sessions are designed to make Python programming accessible to all members of the centre, regardless of their prior experience in Python or programming in general.

## Seminar Schedule

The seminar series is scheduled as follows, with each session focusing on key aspects of Python programming:

- **17 Nov (13-14):** Introduction, visualization and plotting
- **24 Nov (13-14):** Statistical analysis
- **8 Dec (13-14):** Task automation
- **15 Dec (13-14):** Wrap-up session

## Session Format

Each of the first three sessions will commence with a brief lecture (10-15 minutes) followed by an interactive hands-on Jupyter notebook exercise session (45-50 minutes). The final session, the wrap-up, will be entirely hands-on, providing an opportunity to address any questions and consolidate learning.

To support a collaborative learning environment, the hands-on sessions will be conducted physically, with tutors available 

- **Oslo, V205:** Morten, Simen K, and Simen S 
- **Tromsø, C329:** Gabriel and Vanda 

Additionally, the lectures will be accessible remotely via [Zoom](https://uio.zoom.us/meeting/u5Mvdu2urDksHdWZshj_m_p6YFhlpWDDSEos/ics?icsToken=98tyKu-vrDouHNaXth6GR_MEAIj4LO7zpnpdgqdqpgztCR9eXROgD8RKPqJINPzj).

## Learning Beyond Sessions

We highly recommend forming study groups and/or allocating extra time between sessions to delve deeper into the notebook exercises, to maximize learning outcomes. 

## Technical solution

You have two main approaches for the technical solution:
- use a browser
- install Python on your own laptop

### Option 1: Browser solution

We have set up a server [python.hylleraas.sigma2.no](https://python.hylleraas.sigma2.no) for the Python introduction seminars.

#### Requirements:
- Feide account
- Membership in the "Hylleraas School" Feide group. **All participants of the Hylleraas School 2023 should already have access to this group**
- If you do not have access, send an email to [simen.reine@kjemi.uio.no](mailto:simen.reine@kjemi.uio.no) requesting an invitation to join the "Hylleraas School" Feide group. 

#### Steps for Session 1:

1. Go to [python.hylleraas.sigma2.no](https://python.hylleraas.sigma2.no) and log in with your Feide user.
2. Download your preferred Notebook for [Session 1](https://hylleraas-python-school.gitlab.io/chapters/lecture1/intro.html)
3. Upload it to the server and open it to start working on it

### Option 2: Install Python on your machine

#### Requirements:
- root or sudo access

#### Steps for Session 1:

1. Install Python by, for exmaple, following the steps below
2. Download your preferred Notebook for [Session 1](https://hylleraas-python-school.gitlab.io/chapters/lecture1/intro.html)
3. Open it with

```bash
jupyter notebook <name-of-notebook>.ipynb
```

#### Installation of Python with Miniconda

Python is a powerful, versatile programming language. 

A simple way to install Python is to installing [Miniconda](https://docs.conda.io/en/latest/miniconda.html). Miniconda is a package and environment manager that simplifies managing Python environments and dependencies, and by default, comes with Python installed.

We recommend you to try out this option at some point for yourselves. 

#### Verification

To verify the installation, open a command line (Command Prompt on Windows, Terminal on Linux/Mac) and type:

```bash
python --version
```

This should display the Python version number.

#### Installing Pip (Package Installer for Python)

Pip is a tool for installing and managing Python packages. It usually comes pre-installed with Python. With Miniconda installed, you can also install it with

```bash
conda install pip
```

#### Installing Python packages

One of the many powers of Python is that it comes with a multitude of different packages. These packaes can be use for a wide variety of purposes. For example, installing the three packages `numpy`, `pandas` and `matplotlib` with pip, simply type

```bash
pip install numpy pandas matplotlib
````


